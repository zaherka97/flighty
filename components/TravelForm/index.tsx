import React from "react";
import styles from "./styles.module.css";

function index() {
  return (
    <div className={`row `}>
      <div
        className={`col-md-10 row ${styles.fromBackgroud} p-4 mx-auto col-12  `}
      >
        <div className="col-md-6 col-12">
          <div className="">
            <span className="h4 text-danger">01</span>
            <span className="ms-2 h4">Where?</span>
          </div>
          <div className="mt-3 row">
            <div className="col-md-6 col-12">
              <label htmlFor="" className="form-label">
                Starting Place
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="city,region"
              />
            </div>
            <div className="col-md-6 col-12 mt-md-0 mt-3">
              <label htmlFor="" className="form-label">
                Destination
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="city,region"
              />
            </div>
          </div>
        </div>
        <hr className="mx-auto w-75 d-md-none d-block mt-md-0 mt-3" />
        <div className="col-md-6 col-12">
          <div className="">
            <span className="h4 text-danger">02</span>
            <span className="ms-2 h4">When?</span>
          </div>
          <div className="mt-3 row">
            <div className="col-md-6 col-12">
              <label htmlFor="" className="form-label">
                Check-in Date
              </label>
              <input
                type={"date"}
                className="form-control"
                placeholder="city,region"
              />
            </div>
            <div className="col-md-6 col-12 mt-md-0 mt-3">
              <label htmlFor="" className="form-label">
                Check-out Date
              </label>
              <input type={"date"} className="form-control" />
            </div>
          </div>
        </div>
        <button className="btn btn-outline-danger mt-2 w-25 mx-auto">
          search -{">"}
        </button>
      </div>
    </div>
  );
}

export default index;
