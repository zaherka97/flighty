import React, { PropsWithChildren } from "react";
import CarouselItem from "./CarouselItem";
import style from "./styles.module.css";

function homeCarousel(props: PropsWithChildren) {
  //***************************************************************************** */
  const { children } = props;
  //***************************************************************************** */
  //***************************************************************************** */
  return (
    <>
      <div
        id="carouselExampleSlidesOnly"
        className="carousel slide carousel-fade"
        data-bs-ride="carousel"
      >
        <div className="carousel-inner">
          <div className={style.cover}>
            {children}
            <button
              className="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExampleSlidesOnly"
              data-bs-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next"
              type="button"
              data-bs-target="#carouselExampleSlidesOnly"
              data-bs-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
          <CarouselItem src="/slider-image/slider1.jpg" />
          <CarouselItem src="/slider-image/slider2.jpg" />
          <CarouselItem src="/slider-image/slider3.jpg" />
        </div>
      </div>
    </>
  );
}

export default homeCarousel;
