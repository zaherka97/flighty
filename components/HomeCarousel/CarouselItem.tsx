import Image from "next/image";
import React from "react";
import { carouselItemProps } from "./types";

function CarouselItem(props: carouselItemProps) {
  //***************************************************************** */
  const { src, width = "600px", height = "300px" } = props;
  //***************************************************************** */
  //***************************************************************** */
  return (
    <div className="carousel-item active">
      <Image src={src} width={width} height={height} layout={"responsive"} />
    </div>
  );
}

export default CarouselItem;
