export type carouselItemProps = {
  src: string;
  width?: string;
  height?: string;
};
