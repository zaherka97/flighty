import React from "react";
import { CardProps } from "./types";
import { renderCards } from "./utilies";

function index(props: CardProps) {
  //*********************************************** */
  const { element } = props;
  const Cards = renderCards(element);
  //*********************************************** */
  //*********************************************** */
  return <div className="row gy-3">{Cards}</div>;
}

export default index;
