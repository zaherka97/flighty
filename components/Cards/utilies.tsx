import { addIndex, map } from "ramda";
import { CardItemProps } from "./types";
import CardItem from "./CardItem";

const iMap = addIndex<CardItemProps, JSX.Element>(map);

export const renderCards = iMap((value, index) => {
  return (
    <div className="col-xl-3 col-md-4 col-sm-6 col-12">
      <CardItem
        src={`/card-image/card${index % 6}.jpg`}
        title={value.title}
        checkinDate={value.checkinDate}
        checkoutDate={value.checkoutDate}
      />
    </div>
  );
});
