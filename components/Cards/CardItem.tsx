import React from "react";
import { CardItemProps } from "./types";

function CardItem(props: CardItemProps) {
  //************************************************************************* */
  const { title = "paris", src, checkinDate, checkoutDate } = props;
  //************************************************************************* */
  //************************************************************************* */
  return (
    <div className="card">
      <img src={src} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text text-muted">Lorem ipsum dolor sit amet.</p>
        <hr className="mx-auto mt-2 " />
        <div className="">
          <span className="text-primary">Date:</span>
          <span className="text-muted ms-2">{checkinDate}</span>
          <span className="ms-2 text-success">-----{">"}</span>
          <span className="text-muted ms-2">{checkoutDate}</span>
        </div>
        <hr className="mx-auto mt-2 " />
        <div className="text-center">
          <button className="btn btn-danger mt-2  ">Book now</button>
        </div>
      </div>
    </div>
  );
}

export default CardItem;
