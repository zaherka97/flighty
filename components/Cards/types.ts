export type CardItemProps = {
  title: string;
  src?: string;
  checkinDate: string;
  checkoutDate: string;
};

export type CardProps = {
  element: CardItemProps[];
};
