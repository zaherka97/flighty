import React, { FC } from "react";
import { sectionContainerProps } from "./types";

const index: FC<sectionContainerProps> = (props) => {
  //********************************************************************************** */
  const { children, color, title } = props;
  //********************************************************************************** */
  //********************************************************************************** */
  return (
    <div style={{ backgroundColor: color }}>
      <div className="container py-3">
        <p className="h2 text-center text-secondary py-3">{title}</p>
        {children}

        <hr className="w-75 mx-auto " />
      </div>
    </div>
  );
};

export default index;
