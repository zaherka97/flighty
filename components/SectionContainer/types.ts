import React, { PropsWithChildren } from "react";
type gray = "#e4e4e4";
type white = "white";
export type sectionContainerProps = PropsWithChildren & {
  color: gray | white;
  title?: string;
};
