import type { NextPage } from "next";
import Cards from "../components/Cards";
import HomeCarousel from "../components/HomeCarousel";
import SectionContainer from "../components/SectionContainer";
import TravelForm from "../components/TravelForm";
const test = {
  title: "syria",
  checkinDate: "2022-1-5",
  checkoutDate: "2022-6-5",
};
const Home: NextPage = () => {
  return (
    <>
      <HomeCarousel>
        <div
          className="position-relative container d-md-block d-none"
          style={{ top: "25%" }}
        >
          <p className="display-1 text-white text-center">
            Let's Journey begine
          </p>
          <TravelForm />
        </div>
      </HomeCarousel>
      <div className="d-md-none d-block">
        <TravelForm />
      </div>
      <SectionContainer color={"#e4e4e4"} title="Explore All Trips">
        <Cards
          element={[
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
            test,
          ]}
        />
      </SectionContainer>
    </>
  );
};

export default Home;
