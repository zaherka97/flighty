import "../styles/globals.css";
import type { AppProps } from "next/app";
import "bootstrap/dist/css/bootstrap.min.css";
import Head from "next/head";
import { useEffect } from "react";

function MyApp({ Component, pageProps }: AppProps) {
  //********************************************************************************** */
  useEffect(() => {
    document !== undefined ? require("bootstrap/dist/js/bootstrap.js") : null;
  }, []);
  //********************************************************************************** */
  //********************************************************************************** */
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <Component {...pageProps} />;
    </>
  );
}

export default MyApp;
